function loadMusique(obj){
   
    $('#jquery_jplayer_1').jPlayer('setMedia', {
       mp3: $(obj).parent().attr("url")
    }).jPlayer("play");
    
    $('.jp-title').html($(obj).parent().attr("titre"));
}

function stop(){
    $('#jquery_jplayer_1').jPlayer("stop");
}
$(document).ready(function(){
    $("#jquery_jplayer_1").jPlayer({
        ready: function (event) {
            player = $(this);
            $(this).jPlayer("setMedia", {
                    title: "Bubble",
                    mp3:$(".musique").first().attr("url")
            });
            $('.jp-title').html($(".musique").first().attr("titre"));
        },
        swfPath: "../../dist/jplayer",
        supplied: "mp3",
        wmode: "window",
        useStateClassSkin: true,
        autoBlur: false,
        smoothPlayBar: true,
        keyEnabled: true,
        remainingDuration: true,
        toggleDuration: true
    });
    



});