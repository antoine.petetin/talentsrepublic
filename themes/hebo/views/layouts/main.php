<!-- Require the header -->
<?php require_once('tpl_header.php')?>

<!-- Include content pages -->
<?php echo $content; ?>

<!-- Require the footer -->
<?php require_once('tpl_footer.php')?>
