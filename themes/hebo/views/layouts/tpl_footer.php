    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <section id="footer">
    <!-- Include the header bar -->
        <?php include_once('footer.php');?>
    </section><!-- /#header -->
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/js/bootstrap-transition.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/js/bootstrap-alert.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/js/bootstrap-modal.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/js/bootstrap-dropdown.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/js/bootstrap-scrollspy.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/js/bootstrap-tab.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/js/bootstrap-tooltip.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/js/bootstrap-popover.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/js/bootstrap-button.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/js/bootstrap-collapse.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/js/bootstrap-carousel.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/js/bootstrap-typeahead.js"></script>   

    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl;?>/js/jplayer/jquery.jplayer.min.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/js/favorimusique.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/js/lecturemusique.js"></script>
    <script type="text/javascript">
        
    $("#showconnect").click(function(){
       $("#espaceConnexion").slideToggle(); 
    });
    $("#espaceConnexion > table > tbody > tr > td > input").click(function(){
       $("#espaceConnexion").slideToggle(); 
    });
    </script>
  </body>
</html>