<?php
/* @var $this AlbumController */
/* @var $model Album */
/* @var $form CActiveForm */
?>
<?php
$baseUrl = Yii::app()->baseUrl;
//echo $baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl . '/themes/hebo/js/choisirMusique.js', CClientScript::POS_END);

if(isset($message)){
    ?>
    <div class="alert alert-error">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Erreur !</strong> <?php echo $message; ?>
    </div>
<?php

}
?>

<div class="form">
<h1>Créer un album</h1>
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'album-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    ));
    ?>

<?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'libellealbum'); ?>
        <?php echo $form->textField($model, 'libellealbum', array('size' => 60, 'maxlength' => 100)); ?>
<?php echo $form->error($model, 'libellealbum'); ?>
    </div>

    <?php
    $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
        'name' => 'Musique',
        'source' => $this->createUrl("album/suggestMusique"),
        'options' => array(
            'minLength' => '1',
        ),
        'htmlOptions' => array(
            'id' => 'Musique',
            'style' => 'width:30%;display:inline-block;',
        )
    ));
    ?>


    <?php echo CHtml::Button("Ajouter", array('onclick' => 'js:validerAjax($("#Musique").val());', 'class' => 'btn btn-primary', 'style' => 'display:inline-block;')); ?>

    <?php
    //remplissage champs caché
    $lesmusiques = "";
    foreach ($model->musiques as $musique) {
        $lesmusiques .= $musique->titre . ";";
    }
    ?>
    <input type="hidden" name="musiqueStock" id="musiqueStock" value='<?php echo $lesmusiques; ?>' >
    <?php echo $form->error($model, 'Musique'); ?>

    <input type="hidden" name="adresse" id="adresse" value="<?php echo Yii::app()->request->hostinfo. Yii::app()->request->baseUrl; ?>" >
    <?php echo $form->error($model, 'Musique'); ?>

    <div id="Musc">
        <table id="tableContient" class="table table-striped table-bordered table-hover"><tr><th>Musiques ajoutées</th></tr>
            <?php
            //remplissage tableau
            if (!$model->isNewRecord) {
                foreach ($model->musiques as $musique) {
                    echo "<tr>";
                    echo "<td style=\"padding-right:1%;\">tg" . $musique->titre . "</td>";
                    echo "<td><div id='deleteButton' style=\"display:inline-block;\" onclick='deleteRow(this)'>X</div></td></tr>";
                }
            }
            ?>  
        </table>
    </div>

    <div class="button-wrapper-wrapper">
        <div class="button-wrapper">
            <div class="button-swag button-upload">
        <?php
        //echo CHtml::submitButton($model->isNewRecord ? "Créer l'album" : "Mettre à jour l'album", array('class'=>'btn')); 
        if ($model->isNewRecord) {

            echo CHtml::ajaxSubmitButton(
                "Créer l'album",
                CHtml::normalizeUrl(array('album/create')),
                    array(
                        'data' => 'js:$(this).parents("form").serialize()',
                        'success' => 'function(data){
                                            $("#content").html(data);
                                          }',
                        'error' => 'function(data){console.log(data);}'
                    ), array(
                'id' => 'ajaxSubmitBtn',
                'name' => 'ajaxSubmitBtn',
                'class'=>'btn'
            ));
        }
        ?>
            <p>Créer l'album</p>
            </div>
        </div>
    </div>
    <p class="note">Les champs marqués d'une astérisque (<span class="required">*</span>) sont obligatoires.</p>

        <?php $this->endWidget(); ?>

</div><!-- form -->
