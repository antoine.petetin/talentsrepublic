<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);

if($code==403)
    $this->renderPartial('error403');
else if($code == 404)
    $this->renderPartial('error404');
else{    
?>
    <h2>Error <?php echo $code; ?></h2>

    <div class="error">
    <?php echo CHtml::encode($message); ?>
    </div>
<?php
}