<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle=Yii::app()->name . ' - Accès refusé';
$this->breadcrumbs=array(
	'Error',
);
?>

<div class="error">
<?php echo CHtml::encode("Vous n'avez pas le droit d'accéder à cette page."); ?>
</div>