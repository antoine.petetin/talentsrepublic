<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle=Yii::app()->name . ' - Accès refusé';
$this->breadcrumbs=array(
	'Error',
);
?>

<div class="error">
<?php echo CHtml::encode("La page demandée n'existe pas ou plus."); ?>
</div>