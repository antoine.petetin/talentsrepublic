<div class="<?php if (Yii::app()->user->isGuest) echo 'span9';else echo 'span4'; ?>">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#actualites" data-toggle="tab">Actualités</a></li>
        <li><a href="#messons" data-toggle="tab">Mes sons</a></li>
        <li><a href="#mesfavoris" data-toggle="tab">Mes favoris</a></li>
        <li><a href="#amis" data-toggle="tab">Amis <?php $nbInvitations = count(Yii::app()->user->getInvitationsamis());
        if($nbInvitations>0)
            echo '('.$nbInvitations.')';
        ?></a></li>
    </ul>

    <div class="tab-content">
        <?php $this->renderPartial('zones/zone1/actualites', array('posts'=>$posts)); ?>
        <?php $this->renderPartial('zones/zone1/messons', array('messons'=>$messons)); ?>
        <?php $this->renderPartial('zones/zone1/mesfavoris', array('musiquesfavoris'=>$musiquesfavoris)); ?>
        <?php $this->renderPartial('zones/zone1/amis'); ?>
    </div>
</div>
