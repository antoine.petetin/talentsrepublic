<div class="tab-pane active" id="actualites">

    <h3>Ecrivez votre message : </h3>
    <textarea id="contenu" style="width:90%; resize: vertical;" name="post"></textarea>
    <?php
    echo CHtml::ajaxSubmitButton('Publier', Yii::app()->createUrl('post/create'), array(
        'type' => 'POST',
        'data' => 'js:{"idutilisateur": ' . Yii::app()->user->idutilisateur . ', "contenu": $("#contenu").val(), "date":getcurrenttime()}',
        'success' => 'js:function(result){'
        . 'result = JSON.parse(result); '
        . 'if(result.codeErreur == true){'
        . 'creerTrPost(result.data, "debut");'
        . '}else{'
        . 'alert("message non publié"); }'
        . '}',
        'error' => 'js:function(result){console.log(result);}'
            ), array('class' => 'btn btn-small btn-primary', 'style' => 'float:right;', 'name' => 'publierMessage'));
    ?>    <table id="tableposts" class="table table-striped table-hover">
        <tbody>

            <?php $this->renderPartial('../post/index', array('posts' => $posts)); ?>
    </table>
</tbody>
<?php
if (!empty($posts)) {
    echo CHtml::hiddenField('posts_lim_inf', Post::LIMIT, array('id' => 'posts_lim_inf'));
    echo CHtml::button('Voir les posts plus anciens ...', array('id' => 'btnVoirPostsPlusAnciens', 'class' => 'btn btn-small'))
    /* echo CHtml::ajaxSubmitButton(
      'Voir les posts plus anciens...', Yii::app()->createUrl("utilisateur/getnposts", array("posts_lim_inf"=>)), array(
      'type' => 'GET',
      'success' => 'js:function(result){
      console.log("ancienne val : "+$("#posts_lim_inf").val());
      $("#posts_lim_inf").val(result.posts_lim_inf);
      console.log("nouvelle val : "+$("#posts_lim_inf").val());

      $("#tableposts tbody").append(result);

      }',
      'error'=>'function(data){console.log(data);}'
      )); */
    ?>
    <script>
        $("#btnVoirPostsPlusAnciens").click(function () {
            $.get("<?php echo Yii::app()->createUrl('utilisateur/getnposts'); ?>",
                    'posts_lim_inf=' + $("#posts_lim_inf").val(),
                    function (result, status, xhr) {
                        if (status == "success") {
                            result = JSON.parse(result);
                            if (result.codeErreur) {
                                $("#posts_lim_inf").val(result.posts_lim_inf);
                                $("#tableposts tbody").append(result.data);
                            } else
                                alert("false");
                        } else {
                            console.log(xhr);
                        }
                    }
            );
        });
    </script>
    <?php
}
?>
</div>

<script id="script">
            function getcurrenttime() {
                var currentdate = new Date();
                return currentdate.getFullYear() + "-"
                        + (currentdate.getMonth() + 1) + "-"
                        + currentdate.getDate() + " "
                        + currentdate.getHours() + ":"
                        + currentdate.getMinutes() + ":"
                        + currentdate.getSeconds();
            }

            function voirTousLesCommentaires(idpost) {
                $("#commentairesDuPost" + idpost).slideDown();
                $("#btnVoirCommentaires" + idpost).hide();
                $("#btnCacherCommentaires" + idpost).show();

            }

            function cacherTousLesCommentaires(idpost) {
                $("#commentairesDuPost" + idpost).slideUp();
                $("#btnVoirCommentaires" + idpost).show();
                $("#btnCacherCommentaires" + idpost).hide();

            }

            

            function creerTrPost(data, pos) {
                var url = "\"<?php echo Yii::app()->createUrl('post/delete'); ?>" + "/" + data.idpost + "\"";

                
                var tr = $("<tr/>");
                tr.attr('idpost',data.idpost);
                var td = $("<td/>");
                var div = $("<div/>");
                var image = $("<img/>");
                image.css({width:'40px', height:'50px'});
                image.attr('src', "<?php echo Yii::app()->baseUrl . '/img/photosprofiles/' . Yii::app()->user->getUrlPhoto(); ?>");
                var boutonSupprimerPost = $("<input/>");
                boutonSupprimerPost.attr('type', 'button');
                boutonSupprimerPost.attr('class', 'btn btn-danger btn-small');
                boutonSupprimerPost.val('Supprimer');
                boutonSupprimerPost.attr('id','deletePost' + data.idpost);
                boutonSupprimerPost.css('float', 'right');
                boutonSupprimerPost.click(function(){supprimerPost(url,data.idpost)});
                
                div.append(image);
                div.append("\t<?php echo Yii::app()->user->getPseudo(); ?>");
                div.append(boutonSupprimerPost);
                td.append(div);
                td.append('<p>'+data.contenu+'</p>');
                td.append('<div style="float:right;font-size:10px;">Publié le ' + data.date + '</div>');
                tr.append(td);
                
                if (pos == "debut")
                    $("#tableposts tbody").prepend(tr);
                else
                    $("#tableposts tbody").append(tr);

            }

            function supprimerPost(url, idpost) {
                //alert("2");
                $.post(url, function (data, status, xhr) {
                    if (status == "success")
                        $('tr[idpost="' + idpost + '"]').remove();
                    else
                        console.log(data);
                });
            }

            $(".commentaires, .btnCacherCommentaires").each(function () {
                $(this).hide();
            });

            function commenter(obj, idpost) {

                var selecteur = "#valeurcommentaire" + idpost;
                $.post(
                        "<?php echo Yii::app()->createUrl("commentaire/create"); ?>",
                        {"idutilisateur": <?php echo Yii::app()->user->idutilisateur; ?>, "idpost": idpost, "contenu": $(selecteur).val(), "date": getcurrenttime()},
                        function (data, status) {
                            if (status == "success") {
                                console.log(data);
                                data = $.parseJSON(data);
                                if (data.codeErreur) {
                                    //On recharge la page
                                    $.get("<?php echo Yii::app()->createUrl("site/index") ?>", function (result, status) {
                                        $("#content").html(result);
                                        //alert("ajouté");
                                    });
                                } else {
                                    alert('pas ok');
                                }




                            } else if (status == "error") {
                                alert("Le commentaire n'a pas été publié !");
                            }

                        }
                );
                //});
            }
            
    $("a").click(function()
    {
        $('div').each(function(){
            if($(this).prop('offsetHeight') < $(this).prop('scrollHeight') && $(this).css("overflow")!="hidden")
            {
                $(this).mCustomScrollbar({scrollInertia: 10});
            }
            
        });
    });
</script>