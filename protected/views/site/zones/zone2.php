<div class="<?php if (Yii::app()->user->isGuest) echo 'span9';else echo 'span6'; ?>" id="zone2">
    <div class="row-fluid">
        <div>
            <ul class="nav nav-tabs">
                <li class="active"><a href="#recommandation" data-toggle="tab">Recommandation</a></li>
                <li><a href="#recents" data-toggle="tab">Récents</a></li>
                <?php
                if(!Yii::app()->user->isGuest){
                ?><li><a href="#playlists" data-toggle="tab">Playlists</a></li><?php
                } ?>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="recommandation"><?php $this->renderPartial('zones/zone2/recommandation', array('recommandations'=>$recommandations)); ?></div>
                <div class="tab-pane" id="recents"><?php $this->renderPartial('zones/zone2/recents', array('musiquesrecentes'=>$musiquesrecentes)); ?></div>
                <div class="tab-pane" id="playlists"><h3>Settings</h3>These are settings, consectetur adipiscing elit. Quisque malesuada aliquam vulputate. Cras pulvinar erat ac velit eleifend porttitor at eu diam. Praesent elit mi, mattis vitae accumsan bibendum, porttitor non neque.</div>
            </div>
        </div>

    </div><!--/row-fluid-->

</div>