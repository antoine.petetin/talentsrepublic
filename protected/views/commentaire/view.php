<?php
/* @var $this CommentaireController */
/* @var $model Commentaire */

$this->breadcrumbs=array(
	'Commentaires'=>array('index'),
	$model->idcommentaire,
);

$this->menu=array(
	array('label'=>'List Commentaire', 'url'=>array('index')),
	array('label'=>'Create Commentaire', 'url'=>array('create')),
	array('label'=>'Update Commentaire', 'url'=>array('update', 'id'=>$model->idcommentaire)),
	array('label'=>'Delete Commentaire', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idcommentaire),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Commentaire', 'url'=>array('admin')),
);
?>

<h1>View Commentaire #<?php echo $model->idcommentaire; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idcommentaire',
		'idutilisateur',
		'idpost',
		'contenu',
		'date',
	),
)); ?>
