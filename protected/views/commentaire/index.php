<?php
/* @var $this CommentaireController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Commentaires',
);

$this->menu=array(
	array('label'=>'Create Commentaire', 'url'=>array('create')),
	array('label'=>'Manage Commentaire', 'url'=>array('admin')),
);
?>

<h1>Commentaires</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
