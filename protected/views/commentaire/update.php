<?php
/* @var $this CommentaireController */
/* @var $model Commentaire */

$this->breadcrumbs=array(
	'Commentaires'=>array('index'),
	$model->idcommentaire=>array('view','id'=>$model->idcommentaire),
	'Update',
);

$this->menu=array(
	array('label'=>'List Commentaire', 'url'=>array('index')),
	array('label'=>'Create Commentaire', 'url'=>array('create')),
	array('label'=>'View Commentaire', 'url'=>array('view', 'id'=>$model->idcommentaire)),
	array('label'=>'Manage Commentaire', 'url'=>array('admin')),
);
?>

<h1>Update Commentaire <?php echo $model->idcommentaire; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>