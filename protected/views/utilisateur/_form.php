<?php
/* @var $this UtilisateurController */
/* @var $model Utilisateur */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        //'id' => 'utilisateur-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data', //A mettre lorsqu'on veut uploader un fichier
        )
    ));
    ?>

    <p class="note">Les champs avec<span class="required">*</span> sont obligatoires.</p>

    <?php echo $form->errorSummary($model, "L'inscription a échoué car il y a des erreurs de saisies : "); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'prenom'); ?>
        <?php echo $form->textField($model, 'prenom', array('size' => 50, 'maxlength' => 50)); ?>
        <?php echo $form->error($model, 'prenom'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'nom'); ?>
        <?php echo $form->textField($model, 'nom', array('size' => 50, 'maxlength' => 50)); ?>
        <?php echo $form->error($model, 'nom'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'pseudo'); ?>
        <?php echo $form->textField($model, 'pseudo', array('size' => 20, 'maxlength' => 20)); ?>
        <?php echo $form->error($model, 'pseudo'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'mail'); ?>
        <?php echo $form->textField($model, 'mail', array('size' => 50, 'maxlength' => 50)); ?>
        <?php echo $form->error($model, 'mail'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'motdepasse'); ?>
        <?php echo $form->passwordField($model, 'motdepasse', array('size' => 20, 'maxlength' => 20, 'value' => '')); ?>
        <?php echo $form->error($model, 'motdepasse'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'villeresidence'); ?>
        <?php
        $this->widget('ext.gplacesautocomplete.GPlacesAutoComplete', array(
            'name' => 'villeresidence[ville]',
            'options' => array(
                'types' => array(
                    '(cities)'
                ),
                'componentRestrictions' => array(
                    'country' => 'fr',
                )
            )
        ));
        echo $form->error($model, 'idvilleresidence');
        ?>
        <input type="hidden" id="ville" name="villeresidence[ville]"/>
        <input type="hidden" id="departement" name="villeresidence[departement]"/>
        <input type="hidden" id="codepostal" name="villeresidence[codepostal]"/>
        <input type="hidden" id="pays" name="villeresidence[pays]"/>
        <input type="hidden" id="codepays" name="villeresidence[codepays]"/>
        <input type="hidden" id="latitude" name="villeresidence[latitude]"/>
        <input type="hidden" id="longitude" name="villeresidence[longitude]"/>
    </div>

        <?php //Pour ajouter le champs d'upload de l'image   ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'image'); ?>
<?php echo CHtml::activeFileField($model, 'image');  // by this we can upload image   ?>
    <?php echo $form->error($model, 'image'); ?>
        <p class="errorMessage">Attention : la taille de l'image ne doit pas excéder 8Mo !</p>
    </div>
        <?php if ($model->isNewRecord != '1') { ?>
        <div class="row">
        <?php echo CHtml::image(Yii::app()->request->baseUrl . '/banner/' . $model->image, "image", array("width" => 200)); ?>
        </div>
        <?php
        }
        ?><div><?php
        echo CHtml::label('Auditeur', 'auditeurtext', array('style' => 'display:inline;'));
        echo CHtml::radioButton('typeutilisateur', false, array(
            'value' => 'auditeur',
            'name' => 'typeutilisateur',
            'checked' => 'checked',
            'uncheckValue' => null,
            'style' => 'margin:20px;'));

        echo CHtml::label('Artiste', 'artistetext', array('style' => 'display:inline;'));
        echo CHtml::radioButton('typeutilisateur', false, array(
            'value' => 'artiste',
            'name' => 'typeutilisateur',
            'uncheckValue' => null,
            'style' => 'margin:20px;'));

        echo CHtml::label('Promoteur', 'promoteurtext', array('style' => 'display:inline;'));
        echo CHtml::radioButton('typeutilisateur', false, array(
            'value' => 'promoteur',
            'name' => 'typeutilisateur',
            'uncheckValue' => null,
            'style' => 'margin:20px;'
        ));

        echo $form->error($model, 'type');
        ?></div>
    <div class="row-buttons">
        <?php
        /* echo CHtml::ajaxSubmitButton($model->isNewRecord ? 'Je m\'inscrs' : 'Modifier mon profil', 
          CHtml::normalizeUrl(array('site/index')), array(
          'data' => 'js:jQuery(this).parents("form").serialize()',
          'success' => 'function(data){
          $("#content").html("plop");
          }',
          'error' => 'function(data){console.log(data);}'
          ), array(
          'id' => 'ajaxSubmitBtnRegisterForm',
          'class' => 'btn',
          ));
         */

        echo CHtml::submitButton($model->isNewRecord ? 'Je m\'inscris' : 'Modifier mon profil', array('class' => 'btn'));
        ?>
    </div>
    <?php
    $this->endWidget();
    ?>

</div><!-- form -->