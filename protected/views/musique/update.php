<?php
/* @var $this MusiqueController */
/* @var $model Musique */

$this->breadcrumbs=array(
	'Musiques'=>array('index'),
	$model->idmusique=>array('view','id'=>$model->idmusique),
	'Update',
);

$this->menu=array(
	array('label'=>'List Musique', 'url'=>array('index')),
	array('label'=>'Create Musique', 'url'=>array('create')),
	array('label'=>'View Musique', 'url'=>array('view', 'id'=>$model->idmusique)),
	array('label'=>'Manage Musique', 'url'=>array('admin')),
);
?>

<h1>Update Musique <?php echo $model->idmusique; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>