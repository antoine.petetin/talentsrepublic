<?php
/* @var $this MusiqueController */


$baseUrl = Yii::app()->baseUrl;
//echo $baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl.'/themes/hebo/js/analyse.js');

if (count($messons) == 0) {
    ?><h3>Vous n'avez pas encore créé de musiques !</h3>
    <table class="table table-striped table-bordered table-hover">
        <tbody></tbody>
    </table>
    <?php
} else {?>
    <h3>Musiques</h3>
    <table class="table table-striped table-bordered table-hover">
        <tbody>
            <?php foreach ($messons as $musique) { ?>
                <tr class="getId" idmusique="<?php echo $musique->idmusique; ?>"><td>
                        <div>
                            <div class="jp-controls musique" titre="<?php echo $musique->titre; ?>" url="<?php echo Yii::app()->request->hostinfo . '/' . Yii::app()->request->baseUrl . '/musique/' . $musique->urlmusique; ?>">
                                <button class="jp-play" onclick="getDureeMusique();loadMusique(this);loadStat();getIdMusique(this);" role="button" tabindex="0">play</button>
                                <button class="jp-stop" onclick="stop();stopStat();"role="button" tabindex="0">stop</button>
                               
                               
                                
                                
                            </div>
                        
                            <?php
                            
                            echo $musique->titre;?>
                            <?php
                                echo CHtml::ajaxSubmitButton(
                                    'Supprimer', Yii::app()->createUrl('musique/delete',array("id"=>$musique->idmusique)),array(
                                    'type' => 'POST',
                                    'success' => 'function(){ $("tr[idmusique='.$musique->idmusique.']").remove();}'
                ),array('class'=>'btn btn-small btn-danger')
                ); ?></tr>
                       
                   
            <?php }
            ?>
        </tbody>
    </table>
    <?php
}
