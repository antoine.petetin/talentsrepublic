<?php
  $baseUrl = Yii::app()->theme->baseUrl; 
  $cs = Yii::app()->getClientScript();
  Yii::app()->clientScript->registerCoreScript('jquery');
?>
	

<div class="form">

<h1>Uploadez votre musique</h1>
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'musique-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data', //A mettre lorsqu'on veut uploader un fichier
        )
    ));
    ?>

    <?php 
    if($msg != '')
    {
        echo ' <div class="alert alert-success">'.$msg.' </div>';
    }
    ?>

    <?php echo $form->errorSummary($model, 'Des erreurs ont été détectées :'); ?>

    <div id="titlerow" class="row-upload">
        <?php echo $form->labelEx($model, 'titre'); ?>
        <?php echo $form->textField($model, 'titre', array('size' => 60, 'maxlength' => 100)); ?>
    </div>

    <div id="listedegenres" class="row-upload">

        <?php
        //Affichage des genres
        if (!empty($model2)) {
            echo '<div class="genres-wrapper"><div class="boxgenres">';
            echo $form->checkBoxList($model, 'genre', CHtml::listData($model2, 'idgenre', 'libellegenre'), array("id" => "inputgenre"));
            echo '</div></div>';
        }
        
        ?>
    </div>
    <div id="buttonsrow" class="buttons-wrapper">
        <div class="big-wrapper">
            <div class="button-wrapper-wrapper">
                <div class="button-wrapper">
                    <div class="button-swag">
                    <?php echo CHtml::activeFileField($model, 'lienmusique');  // by this we can upload music    ?>
                      <p>Ajouter une musique</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="big-wrapper">
            <div class="button-wrapper-wrapper">
                <div class="button-wrapper">
                    <div class="button-swag button-upload">
                    <?php
                    echo CHtml::submitButton($model->isNewRecord ? 'Uploader' : 'Mettre à jour la musique', array('class' => 'btn'));
                    ?>
                      <p>Upload</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div class="text-wrapper">
            <p class="errorMessage">Attention : la taille de du fichier ne doit pas excéder 20Mo !</p>
        </div>
    
    <?php $this->endWidget(); ?>

    <p class="note">Les champs marqués d'une astérisque (<span class="required">*</span>) sont obligatoires.</p>
</div><!-- form -->

<link rel="stylesheet" href="<?php echo $baseUrl;?>/js/malihuscrollbars/jquery.mCustomScrollbar.min.css" />
<script type="text/javascript" src="<?php echo $baseUrl;?>/js/malihuscrollbars/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript">
    $(document).ready(function()
    {            
        $('.genres-wrapper').each(function(){
            var $this = $(this);
            var sum=20;
            $this.children().each(function(){
                sum +=parseInt($(this).css("height")); 
            });
            if (parseInt($this.css("height"))<sum)
            {
                $this.css("max-height", ($("#content").height()));

                    $this.css("overflow", "hidden");
                    $this.mCustomScrollbar({scrollInertia: 10});
            }
        });   
    });
</script>
