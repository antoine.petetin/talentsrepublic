<?php
/* @var $this MusiqueController */
/* @var $model Musique */
?>

<h1>View Musique #<?php echo $model->idmusique; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idmusique',
		'titre',
	),
)); ?>
