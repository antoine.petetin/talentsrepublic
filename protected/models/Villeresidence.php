<?php

/**
 * This is the model class for table "villeresidence".
 *
 * The followings are the available columns in table 'villeresidence':
 * @property integer $idvilleresidence
 * @property string $ville
 * @property string $departement
 * @property string $codepostal
 * @property string $pays
 * @property string $codepays
 * @property string $latitude
 * @property string $longitude
 */
class Villeresidence extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'villeresidence';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ville, departement, pays, codepays', 'required'),
			array('ville, departement, pays', 'length', 'max'=>100),
			array('codepostal', 'length', 'max'=>10),
			array('codepays', 'length', 'max'=>5),
			array('latitude, longitude', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idvilleresidence, ville, departement, codepostal, pays, codepays, latitude, longitude', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idvilleresidence' => 'Idvilleresidence',
			'ville' => 'Ville',
			'departement' => 'Departement',
			'codepostal' => 'Codepostal',
			'pays' => 'Pays',
			'codepays' => 'Codepays',
			'latitude' => 'Latitude',
			'longitude' => 'Longitude',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idvilleresidence',$this->idvilleresidence);
		$criteria->compare('ville',$this->ville,true);
		$criteria->compare('departement',$this->departement,true);
		$criteria->compare('codepostal',$this->codepostal,true);
		$criteria->compare('pays',$this->pays,true);
		$criteria->compare('codepays',$this->codepays,true);
		$criteria->compare('latitude',$this->latitude,true);
		$criteria->compare('longitude',$this->longitude,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Villeresidence the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
