<?php

/**
 * This is the model class for table "musique".
 *
 * The followings are the available columns in table 'musique':
 * @property integer $idmusique
 * @property string $titre
 * @property string $urlmusique
 * @property integer $idutilisateur
 * @property string $dateajout
 * @property double $note
 */
class Musique extends CActiveRecord
{
        public $lienmusique;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'musique';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('titre, urlmusique', 'required', 'message' => 'Le champs {attribute} est obligatoire !'),
                        array('idutilisateur', 'numerical', 'integerOnly' => true),
			array('note', 'numerical'),
                        array('titre', 'length', 'max' => 100),
                        array('urlmusique', 'length', 'max' => 100000),
                        array('lienmusique', 'EImageValidator', 'min_size' => 0, 'max_size' => 20000, 'allowEmpty' => true, 'sizeError' => 'La taille du fichier est trop grande (>20M) !'),
                        array('lienmusique', 'file', 'types' => 'mp3, wav, MP3, WAV', 'allowEmpty' => true, 'on' => 'insert,update', 'message' => 'Le type de fichier doit être mp3 ou wav !'), // this will allow empty field when page is update (remember here i create scenario update)
                        array('dateajout', 'safe'),
                        // The following rule is used by search().
                        // @todo Please remove those attributes that should not be searched.
                        array('idmusique, titre, urlmusique, idutilisateur, dateajout', 'safe', 'on' => 'search'),
       );
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'genre'=>array(self::MANY_MANY, 'Genre', 'musiquegenre(idmusique, idgenre)'),
                    'artiste'=>array(self::BELONGS_TO, 'Utilisateur', 'idutilisateur'),
                    'auteur'=>array(self::BELONGS_TO, 'Utilisateur', 'idutilisateur'),
                    'albums'=>array(self::MANY_MANY, 'Album', 'appartient(idmusique,idalbum)')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idmusique' => 'Idmusique',
			'titre' => 'Titre',
			'urlmusique' => 'Urlmusique',
			'idutilisateur' => 'Idutilisateur',
			'dateajout' => 'Dateajout',
			'note' => 'Note',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idmusique',$this->idmusique);
		$criteria->compare('titre',$this->titre,true);
		$criteria->compare('urlmusique',$this->urlmusique,true);
		$criteria->compare('idutilisateur',$this->idutilisateur);
		$criteria->compare('dateajout',$this->dateajout,true);
		$criteria->compare('note',$this->note);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Musique the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
    
        public function estDansMesFavoris($idutilisateur){
            $utilisateur = Aenfavorismusique::model()->findByAttributes(array('idmusique'=>$this->idmusique, 'idutilisateur'=>$idutilisateur));
            return $utilisateur!=null;
        }
        
        public static function getMusiquesFavoris($idutilisateur){
            $aenfavoris = Aenfavorismusique::model()->findAllByAttributes(array('idutilisateur'=>$idutilisateur));
            $idmusiques = array();
            foreach($aenfavoris as $fav){
                $idmusiques[] = $fav->idmusique;
            }
            $criteria = new CDbCriteria();
            $criteria->addInCondition("idmusique", $idmusiques);
            return self::model()->findAll($criteria);
        } 
}
