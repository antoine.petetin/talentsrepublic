<?php

class MusiqueController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'delete', 'update', 'ajouterEnFavori', 'supprimerDeMesFavoris', 'getMusic', 'search', 'calculStat', 'modifierNote', 'modifierEcoute'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete', 'statMusique'),
                'expression' => 'Yii::app()->user->isAdmin()',
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /* Liste toutes les musiques de l'artiste */

    public function actionMesSons() {
        $idutil = Yii::app()->user->idutilisateur;

        $criteria1 = new CDbCriteria();
        $criteria1->alias = 'm';
        $criteria1->condition = 'm.numeroutilisateur=' . $idutil;
        $criteria1->order = 'titre ASC';

        //Récupération des recettes de l'utilisateur
        $dataProvider = new CActiveDataProvider('Musique', array('criteria' => $criteria1));

        $this->renderPartial('messons', array('dataProvider' => $dataProvider));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {

        $model = new Musique;
        $model2 = new Genre;
        $msg = '';

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Musique'])) {

            $model->attributes = $_POST['Musique'];
            $model->dateajout = date("Y-m-d H:i:s"); //Obligé car MySQL 5.7 n'accepte pas les CURRENT_TIMESTAMP 
            $model->idutilisateur = Yii::app()->user->idutilisateur;

            // Pour l'upload de la musique
            $rnd = rand(0, 9999);  // generate random number between 0-9999
            $uploadedFile = CUploadedFile::getInstance($model, 'lienmusique');
            //var_dump($uploadedFile);
            $fileName = "{$rnd}-{$uploadedFile}";  // random number + file name                        
            if ($uploadedFile != null) {
                $model->lienmusique = $fileName;
                $model->urlmusique = $fileName;
            }

            if ($model->save()) {

                if ($uploadedFile != null) {
                    $uploadedFile->saveAs(Yii::app()->basePath . '/../musique/' . $model->idmusique . substr($model->urlmusique,strrpos($model->urlmusique, '.')));
                    $model->urlmusique = $model->idmusique . substr($model->urlmusique,strrpos($model->urlmusique, '.'));
                    $model->save();
                }

                if (!empty($_POST["Musique"]["genre"])) {
                    for ($i = 0; $i < count($_POST["Musique"]["genre"]); $i++) {
                        $$i = new Musiquegenre;
                        $$i->idmusique = $model->idmusique;
                        $$i->idgenre = $_POST["Musique"]["genre"][$i];
                        $$i->save();
                    }
                }
            }
            $msg = 'La musique à été ajoutée !';
        }

        $model = new Musique;
        $model2 = Genre::model()->findAll();

        if (Yii::app()->request->isAjaxRequest)
            $this->renderPartial('create', array(
                'model' => $model, 'model2' => $model2, 'msg' => $msg
            ));
        else
            $this->render('create', array(
                'model' => $model, 'model2' => $model2, 'msg' => $msg
            ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Musique'])) {
            $model->attributes = $_POST['Musique'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->idmusique));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $musique = $this->loadModel($id);
        if ($musique != null) {
            Appartient::model()->deleteAll('idmusique=:id', array('id'=>$id));
            Musiquegenre::model()->deleteAll('idmusique=:id', array('id'=>$id));
            Nbecoute::model()->deleteAll('idmusique=:id', array('id'=>$id));
            Aenfavorismusique::model()->deleteAll('idmusique=:id', array('id'=>$id));
            
            //On supprime le fichier du serveur
            $musique->delete();
            if (file_exists($_SERVER['DOCUMENT_ROOT'].Yii::app()->baseUrl . '/musique/' . $musique->urlmusique))
                unlink($_SERVER['DOCUMENT_ROOT'].Yii::app()->baseUrl . '/musique/' . $musique->urlmusique);
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Musique');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionAjouterEnFavori() {
        $res = array();
        if (Yii::app()->request->isAjaxRequest) {
            if (isset($_POST['idutilisateur']) && isset($_POST['idmusique'])) {
                if (Aenfavorismusique::model()->findByAttributes(array('idmusique' => $_POST['idmusique'], 'idutilisateur' => $_POST['idutilisateur'])) == null) {
                    $fav = new Aenfavorismusique();
                    $fav->idmusique = $_POST['idmusique'];
                    $fav->idutilisateur = $_POST['idutilisateur'];
                    $res['codeErreur'] = $fav->save();
                    if ($res['codeErreur'])
                        $res['message'] = "Ajouté en favori !";
                    else
                        $res['message'] = "La musique n'a pas été ajoutée en favori !";
                }else {
                    $res['codeErreur'] = false;
                    $res['message'] = "Ajouté en favori !";
                }
            } else {
                $res['codeErreur'] = false;
                $res['message'] = "La musique n'a pas été ajouté en favori !" . print_r($_POST);
            }
        }

        echo json_encode($res, true);
    }

    public function actionSupprimerDeMesFavoris() {
        $res = array();
        if (Yii::app()->request->isAjaxRequest) {
            if (isset($_POST['idutilisateur']) && isset($_POST['idmusique'])) {
                $fav = Aenfavorismusique::model()->findByAttributes(array('idmusique' => $_POST['idmusique'], 'idutilisateur' => $_POST['idutilisateur']));
                if ($fav != null) {
                    $res['codeErreur'] = $fav->delete();
                    if ($res['codeErreur'])
                        $res['message'] = "La musique a été supprimée de mes favoris !";
                    else
                        $res['message'] = "La musique n'a pas été supprimée de mes favoris !";
                }else {
                    $res['codeErreur'] = true;
                    $res['message'] = "La musique a été supprimée de mes favoris !";
                }
            } else {
                $res['codeErreur'] = false;
                $res['message'] = "La musique n'a pas été supprimée de mes favoris !";
            }
        } else {
            $res['codeErreur'] = false;
            $res['message'] = "La musique n'a pas été supprimée de mes favoris !";
        }

        echo json_encode($res, true);
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Musique('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Musique']))
            $model->attributes = $_GET['Musique'];

        $this->renderPartial('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Musique the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Musique::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Musique $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'musique-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionGetMusic() {
        $idutil = Yii::app()->user->idutilisateur;

        $criteria1 = new CDbCriteria();
        $criteria1->condition = 'idutilisateur=' . $idutil;
        $criteria1->addcondition(" titre='" . $_GET["titre"] . "'");

        $model2 = Musique::model()->findAll($criteria1);
        echo CJSON::encode($model2);
    }

    public function actionSearch() {
        if(isset($_POST['recherche']))
        { 
            $recherche = $_POST['recherche'];
        }
        else
        {
            $recherche = $_POST['Musique'];            
        }
        if (!empty($recherche)) {

            $res = $recherche;

            $criteria1 = new CDbCriteria();
            $criteria1->alias = 'm';
            $criteria1->condition = "m.titre LIKE '%" . $res . "%'";
            $criteria1->order = 'titre ASC';

            $criteria2 = new CDbCriteria();
            $criteria2->alias = 'u';
            $criteria2->condition = "u.pseudo LIKE '%" . $res . "%' AND u.typeutilisateur = 'artiste' ";
            $criteria2->order = 'u.pseudo ASC';

            //Récupération des recettes de l'utilisateur
            $musiquestrouvees = Musique::model()->findAll($criteria1);
            $artistestrouves = Utilisateur::model()->findAll($criteria2);


            $this->renderPartial('../site/result', array(
                'musiquestrouvees' => $musiquestrouvees,
                'artistestrouves' => $artistestrouves,
                    ), false, true);
        } else {
            $this->render('../site/result', array('artistestrouves'=>array()), false, true);
        }
    }

    public function actionCalculStat() {
        //RECUPERATION DES DONNEES NECESSAIRES

        $nbPause = json_decode($_GET['nbPause']);
        $nbMove = json_decode($_GET['nbMove']);
        $tempsTot = $_GET['tempsEcoute'];
        $idMusc = $_GET['idMusique'];
        $dureeMusc = $_GET['dureeMusc'];
        $tpsAvantPremierClic = $_GET['tpsAvantPremierClic'];
        $repeat = $_GET['repeat'];

        //preparation de l'ajout de l'écoute
        $ecoute = new Nbecoute;
        $ecoute->idmusique = $idMusc;
        $ecoute->nb = 1;
        $ecoute->dateecoute = date("Y-m-d");



        $criteria = new CDbCriteria(array(
            'condition' => "idmusique = :id",
            'params' => array(':id' => $idMusc)
        ));

        $model = Musique::model()->find($criteria);

        //CALCUL NOTE
        //COTE POSITIF

        $note = $model->note;
        if($dureeMusc!=0)
        {
            //si > 50% de la musique avant le premier clic
            if ($tpsAvantPremierClic > $dureeMusc / 2 && $tpsAvantPremierClic != 0) {
                //$note = $note + (($tpsAvantPremierClic / $dureeMusc) * 100 - 50) / 100;
            }




            //si > 50% de la musique
            if ($tempsTot > $dureeMusc / 2) {
                //si c'est une réécoute
                if ($repeat == true) {
                    $note = $note + ((($tempsTot / $dureeMusc) * 100 - 50) / 100) * 2;
                } else {
                    $note = $note + (($tempsTot / $dureeMusc) * 100 - 50) / 100;
                }

                //si > 90% de la musique
                if (($tempsTot / $dureeMusc) * 100 > 90) {
                    $note = $note + ((($tempsTot / $dureeMusc) * 100 - 90) / 10) / 4;
                }

                $ecoute->estpositif = 1;
            } else {
                $ecoute->estpositif = 0;
            }


            //pour chaque déplacement
            foreach ($nbMove as $v) {
                //on regarde si retour en arriere il y a
                if ($v[0] > $v[1]) {
                    var_dump($nbMove);
                    $note = $note + (($v[0] - $v[1]) / $dureeMusc) / 3;
                }
            }


            //COTE NEGATIF
            //si > 50% de la musique avant le premier clic
            if ($tempsTot < $dureeMusc / 2) {
                $note = $note + (($tempsTot / $dureeMusc) * 100 - 50) / 100;
            }

            //nombre de clic (sauf play du début et stop)
            $nbClic = count($nbPause) + count($nbMove);
            $note = $note - ($nbClic / $dureeMusc) * 1.1;


            //pour chaque déplacement
            foreach ($nbMove as $v) {
                //on regarde si avancement dans la musique il y a
                if ($v[0] < $v[1]) {
                    $note = $note - (($v[0] - $v[1]) / $dureeMusc) / 2;
                }
            }

            //si écoute de - de 5% de la musique
            if (($tempsTot / $dureeMusc) * 100 < 5) {
                $note = $note - 2 * (0.5 - ($tempsTot / $dureeMusc));
            }

            //MAJ DE LA BASE DE DONNEES

            $res = self::actionModifierNote($note, $model);
            $res2 = self::actionModifierEcoute($ecoute);

            echo json_encode($res && $res2);
        }
    }

    private static function actionModifierNote($note, $musique) {
        $musique->note = round($note, 2);
        return $musique->update();
    }

    private static function actionModifierEcoute($ecoute) {

        $criteria = new CDbCriteria(array(
            'condition' => "dateecoute = :dateecoute AND estpositif = :estpositif",
            'params' => array(':dateecoute' => $ecoute->dateecoute, ':estpositif' => $ecoute->estpositif)
        ));

        $result = Nbecoute::model()->find($criteria);
        if ($result != NULL) {
            $result->nb += 1;
            return $result->update();
        } else {
            return $ecoute->save();
        }
    }

    public function actionStatMusique($idmusique) {
        if ($idmusique != -1) {
            
            $criteria1 = new CDbCriteria();
            $criteria1->alias = 's';
            $criteria1->condition = 's.idmusique=' . $idmusique . ' AND s.estpositif=1';
            $criteria1->order = 's.dateecoute ASC';

            $criteria2 = new CDbCriteria();
            $criteria2->alias = 's';
            $criteria2->condition = 's.idmusique=' . $idmusique . ' AND s.estpositif=0';
            $criteria2->order = 's.dateecoute ASC';
            
            
            $good_format = strtotime (date("Y-m-d"));
            
            
            $criteria3 = new CDbCriteria();
            $criteria3->alias = 's';
            $criteria3->condition = 's.idmusique=' . $idmusique . ' AND s.estpositif=1 AND WEEK(s.dateecoute)=' . date('W',$good_format);
            $criteria3->order = 's.dateecoute ASC';
            
            $criteria4 = new CDbCriteria();
            $criteria4->alias = 's';
            $criteria4->condition = 's.idmusique=' . $idmusique . ' AND s.estpositif=0 AND WEEK(s.dateecoute)=' . date('W',$good_format);
            $criteria4->order = 's.dateecoute ASC';

            $criteria5 = new CDbCriteria();
            $criteria5->alias = 's';
            $criteria5->select = 'SUM(s.nb) as nb, DATE_FORMAT(s.dateecoute,"%U") as dateecoute';
            $criteria5->condition = 's.idmusique=' . $idmusique . ' AND s.estpositif=1';
            $criteria5->order = 's.dateecoute ASC';
            $criteria5->group = 'WEEK(s.dateecoute)';
            $criteria5->limit = '4';
            
            $criteria6 = new CDbCriteria();
            $criteria6->alias = 's';
            $criteria6->select = 'SUM(s.nb) as nb, DATE_FORMAT(s.dateecoute,"%U") as dateecoute';
            $criteria6->condition = 's.idmusique=' . $idmusique . ' AND s.estpositif=0';
            $criteria6->order = 's.dateecoute ASC';
            $criteria6->group = 'WEEK(s.dateecoute)';
            $criteria6->limit = '4';
            
            $criteria7 = new CDbCriteria();
            $criteria7->alias = 's';
            $criteria7->select = 'SUM(s.nb) as nb, DATE_FORMAT(s.dateecoute,"%M") as dateecoute';
            $criteria7->condition = 's.idmusique=' . $idmusique . ' AND s.estpositif=1';
            $criteria7->order = 's.dateecoute ASC';
            $criteria7->group = 'MONTH(s.dateecoute)';
            $criteria7->limit = '4';
            
            $criteria8 = new CDbCriteria();
            $criteria8->alias = 's';
            $criteria8->select = 'SUM(s.nb) as nb, DATE_FORMAT(s.dateecoute,"%M") as dateecoute';
            $criteria8->condition = 's.idmusique=' . $idmusique . ' AND s.estpositif=0';
            $criteria8->order = 's.dateecoute ASC';
            $criteria8->group = 'MONTH(s.dateecoute)';
            $criteria8->limit = '4';
            
            $criteria9 = new CDbCriteria();
            $criteria9->alias = 's';
            $criteria9->select = 'SUM(s.nb) as nb, s.dateecoute';
            $criteria9->condition = 's.idmusique=' . $idmusique;
            $criteria9->order = 's.dateecoute ASC';
            $criteria9->group = 'DAY(s.dateecoute)';
            
            $criteria10 = new CDbCriteria();
            $criteria10->alias = 's';
            $criteria10->condition = 's.idmusique=' . $idmusique . ' AND WEEK(s.dateecoute)=' . date('W',$good_format);
            $criteria10->order = 's.dateecoute ASC';

            $criteria11 = new CDbCriteria();
            $criteria11->alias = 's';
            $criteria11->select = 'SUM(s.nb) as nb, DATE_FORMAT(s.dateecoute,"%U") as dateecoute';
            $criteria11->condition = 's.idmusique=' . $idmusique;
            $criteria11->order = 's.dateecoute ASC';
            $criteria11->group = 'WEEK(s.dateecoute)';
            $criteria11->limit = '4';
            
            
            $criteria12 = new CDbCriteria();
            $criteria12->alias = 's';
            $criteria12->select = 'SUM(s.nb) as nb, DATE_FORMAT(s.dateecoute,"%M") as dateecoute';
            $criteria12->condition = 's.idmusique=' . $idmusique;
            $criteria12->order = 's.dateecoute ASC';
            $criteria12->group = 'MONTH(s.dateecoute)';
            $criteria12->limit = '4';

            //Récupération des écoutes de la musique
            $dataProvider1 = new CActiveDataProvider('Nbecoute', array('criteria' => $criteria1));
            $dataProvider2 = new CActiveDataProvider('Nbecoute', array('criteria' => $criteria2));
            $dataProvider3 = new CActiveDataProvider('Nbecoute', array('criteria' => $criteria3));
            $dataProvider4 = new CActiveDataProvider('Nbecoute', array('criteria' => $criteria4));
            $dataProvider5 = Nbecoute::model()->findAll($criteria5);
            $dataProvider6 = Nbecoute::model()->findAll($criteria6);
            $dataProvider7 = Nbecoute::model()->findAll($criteria7);
            $dataProvider8 = Nbecoute::model()->findAll($criteria8);
            
            //toutes les ecoutes
            
            $dataProvider9 = Nbecoute::model()->findAll($criteria9);
            $dataProvider10 = new CActiveDataProvider('Nbecoute', array('criteria' => $criteria10));
            $dataProvider11 = Nbecoute::model()->findAll($criteria11);
            $dataProvider12 = Nbecoute::model()->findAll($criteria12);
            
            
            //$data2 = $dataProvider2->getData();
           // var_dump($dataProvider5);
            $this->render('stat', array('dataProvider1' => $dataProvider1,
                'dataProvider2' => $dataProvider2,
                'dataProvider3' => $dataProvider3,
                'dataProvider4' => $dataProvider4,
                'dataProvider5' => $dataProvider5,
                'dataProvider6' => $dataProvider6,
                'dataProvider7' => $dataProvider7,
                'dataProvider8' => $dataProvider8,
                'dataProvider9' => $dataProvider9,
                'dataProvider10' => $dataProvider10,
                'dataProvider11' => $dataProvider11,
                'dataProvider12' => $dataProvider12
                    ));
        }
    }

}
