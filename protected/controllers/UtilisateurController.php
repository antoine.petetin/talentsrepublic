<?php

class UtilisateurController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }
    public function actionMesSons($id) {
        $sql = "SELECT m.idmusique, m.titre, m.urlmusique, m.dateajout,  al.libellealbum, g.libellegenre FROM musique m LEFT JOIN appartient AS ap ON m.idmusique = ap.idmusique LEFT JOIN album AS al ON al.idalbum = ap.idalbum LEFT JOIN musiquegenre as mg ON mg.idmusique=m.idmusique LEFT JOIN genre as g ON g.idgenre=mg.idgenre WHERE m.idutilisateur =:value ORDER BY m.dateajout DESC ";
        $params = array(":value"=>$id);
        Yii::app()->clientScript->scriptMap['*.js'] = false;
        return Musique::model()->findAllBySql($sql, $params);
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'create', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('update', 'ajouterami', 'supprimerami', 'accepterami', 'refuserami', 'desactivercompte', 'mesSons', 'getNPosts'),
                'users' => array('@')
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete', 'statMusique'),
                'expression'=>'Yii::app()->user->isAdmin()',
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        if (!Yii::app()->user->isGuest) {

            $invitationEnvoyee = Estamis::model()->findByAttributes(array('idutilisateur' => Yii::app()->user->idutilisateur, 'idutilisateur2' => $id)) != null;
            $invitationAcceptee = Estamis::model()->findByAttributes(array('idutilisateur2' => Yii::app()->user->idutilisateur, 'idutilisateur' => $id)) != null;
            $messons = $this->actionMesSons($id);
            if (Yii::app()->request->isAjaxRequest) {
                $this->renderPartial('view', array(
                    'model' => $this->loadModel($id),
                    'messons' => $messons,               
                    'invitationenvoyee' => $invitationEnvoyee,
                    'estdejaamis' => $invitationEnvoyee && $invitationAcceptee //Les 2 sont amis
                        ), false, true); //Le dernier paramètre est important car sans lui, l'appel ajax ami ne fonctionne pas !!!
            } else {
                $this->render('view', array(
                    'model' => $this->loadModel($id),
                    'messons' => $messons,                    
                    'invitationenvoyee' => $invitationEnvoyee,
                    'estdejaamis' => $invitationEnvoyee && $invitationAcceptee //Les 2 sont amis
                ));
            }
        }else{
            if (Yii::app()->request->isAjaxRequest) {
                $this->renderPartial('view', array(
                    'model' => $this->loadModel($id)
                        ), false, true); //Le dernier paramètre est important car sans lui, l'appel ajax ami ne fonctionne pas !!!
            } else {
                $this->render('view', array(
                    'model' => $this->loadModel($id)
                ));
            }
        }
    }

    public function actionGetNPosts() {
        $result = array();
        $result["codeErreur"] = true;
        if(isset($_GET["posts_lim_inf"])){
            $result["data0"] = Yii::app()->user->getPosts($_GET["posts_lim_inf"]);
            $result["posts_lim_inf"] = $_GET["posts_lim_inf"]+Post::LIMIT;
        }else{
            $result["data0"] = Yii::app()->user->getPosts(0);
            $result["posts_lim_inf"] = 0;
        }
        $result["data"] = $this->renderPartial('../post/index',array('posts'=>$result["data0"]), true);
        unset($result["data0"]);
        echo json_encode($result);
    }
    
    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {

        $model = new Utilisateur;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Utilisateur']) && isset($_POST['villeresidence'])) {
            //On regarde si la ville de résidence est bonne
            if (trim($_POST['villeresidence']['longitude']) != "") {

                if (isset($_POST['typeutilisateur'])) {
                    $model->attributes = $_POST['Utilisateur'];
                    //On encode juste le mot de passe en MD5
                    $model->motdepasse = md5($model->motdepasse);

                    $typeutilisateur = $_POST['typeutilisateur'];
                    if ($typeutilisateur == 'artiste' || $typeutilisateur == 'promoteur')
                        $model->typeutilisateur = $typeutilisateur;
                    else
                        $model->typeutilisateur = 'auditeur';

                    //Pour l'image
                    $rnd = rand(0, 9999);  // generate random number between 0-9999
                    $uploadedFile = CUploadedFile::getInstance($model, 'image');
                    if ($uploadedFile != null) {
                        if ($uploadedFile->size > 8000000) {
                            $model->addError('image', "La taille de l'image doit être inférieure à 8Mo");
                        } else {
                            $fileName = "{$rnd}-{$uploadedFile}";  // random number + file name
                            $fileName = self::normalizeChars($fileName);
                            $model->image = $fileName;
                            $model->urlphoto = $fileName;
                        }
                    }

                    $villeresidenceCriterias = $_POST['villeresidence'];
                    $villeresidence = Villeresidence::model()->findByAttributes($villeresidenceCriterias);
                    //Si la ville de résidence n'existe pas, on la créé
                    if ($villeresidence == null) {
                        $villeresidence = new Villeresidence();
                        $villeresidence->attributes = $villeresidenceCriterias;
                        $villeresidence->save();
                    }
                    $model->idvilleresidence = $villeresidence->idvilleresidence;

                    if ($model->save()) {

                        if ($uploadedFile != null && $uploadedFile->size < 8000000)
                            $uploadedFile->saveAs(Yii::app()->basePath . '/../img/photosprofiles/' . self::normalizeChars($fileName));

                        //On se connecte automatiquement
                        $identity = new UserIdentity($model->mail, $model->motdepasse);
                        $identity->authenticate();
                        Yii::app()->user->login($identity, 0);

                        $this->redirect(array('view', 'id' => $model->idutilisateur));
                    }
                } else
                    $model->addError('type', "Veuillez sélectionner un type !");
            } else
                $model->addError('idvilleresidence', "La ville de résidence est incorrecte !");
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        if (Yii::app()->user->idutilisateur != $id)
            throw new CHttpException(403, 'Vous ne pouvez modifier que votre propre compte !');

        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Utilisateur'])) {
            $model->attributes = $_POST['Utilisateur'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->idutilisateur));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Utilisateur');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionAjouterami() {
        if (Yii::app()->request->isAjaxRequest) {
            if (!Yii::app()->user->isGuest) {
                $ami = new Estamis();
                $ami->idutilisateur = Yii::app()->user->idutilisateur;
                $ami->idutilisateur2 = $_POST['idami'];
                $ami->demandeenattente = 1;

                $res = array('status' => $ami->save(), 'message');
                if ($res['status'] == true)
                    $res['message'] = 'Demande d\'ajout envoyée !';
                else
                    $res['message'] = 'Erreur lors de l\'ajout en amis !';

                echo json_encode($res);
            }
        }
    }

    public function actionAccepterami() {
        if (Yii::app()->request->isAjaxRequest) {
            if (!Yii::app()->user->isGuest) {
                $ami = new Estamis();
                $ami->idutilisateur = Yii::app()->user->idutilisateur;
                $ami->idutilisateur2 = $_POST['idami'];
                $ami->demandeenattente = 0;

                //On met aussi à jour la demande d'ami
                $ami2 = Estamis::model()->findByAttributes(array('idutilisateur' => $_POST['idami'], 'idutilisateur2' => Yii::app()->user->idutilisateur));
                if ($ami2 != null) {
                    $ami2->demandeenattente = 0;
                    $res = array('status' => $ami->save() && $ami2->update(), 'message');
                    if ($res['status'])
                        $res['message'] = 'Vous êtes maintenant amis !';
                    else
                        $res['message'] = 'Erreur lors de la confirmation !';
                }else {
                    $res = array('status' => false, 'message' => 'Erreur lors la confirmation');
                }
                echo json_encode($res);
            }
        }
    }

    public function actionSupprimerami() {
        if (Yii::app()->request->isAjaxRequest) {
            if (!Yii::app()->user->isGuest) {
                $ami = Estamis::model()->findByAttributes(array('idutilisateur' => Yii::app()->user->idutilisateur, 'idutilisateur2' => $_POST['idami']));
                $ami2 = Estamis::model()->findByAttributes(array('idutilisateur2' => Yii::app()->user->idutilisateur, 'idutilisateur' => $_POST['idami']));

                if ($ami2 != null)
                    $res = array('status' => $ami->delete() && $ami2->delete(), 'message');
                else
                    $res = array('status' => $ami->delete(), 'message');

                if ($res['status'])
                    $res['message'] = 'Retiré de la liste d\'amis';
                else
                    $res['message'] = 'Echec lors de la suppression dans votre liste d\'amis !';

                echo json_encode($res);
            }
        }
    }

    public function actionRefuserami() {
        if (Yii::app()->request->isAjaxRequest) {
            if (!Yii::app()->user->isGuest) {
                $ami = Estamis::model()->findByAttributes(array('idutilisateur' => Yii::app()->user->idutilisateur, 'idutilisateur2' => $_POST['idami']));
                if ($ami != null) {
                    $res = array('status' => $ami->delete(), 'message');
                    if ($res['status'])
                        $res['message'] = 'Vous avez refusé cette invitation !';
                    else
                        $res['message'] = "Echec lors du refus de l'invitation !";
                }else {
                    $res = array('status' => false, 'message' => "Echec lors du refus de l'invitation !");
                }
                echo json_encode($res);
            }
        }
    }

    public function actionDesactiverCompte() {
        if (!Yii::app()->user->isGuest && isset($_GET['id'])) {
            //On désactive le compte en passant le champs active à 0
            $model = Utilisateur::model()->findByPk($_GET['id']);
            if ($model != null) {
                $model->active = 0;
                if (!$model->update()) {
                    $model->addError('active', "Votre n'a pas été désactivé !");
                } else {
                    //On déconnecte l'utilisateur
                    Yii::app()->user->logout();
                }
                $this->render('view', array('model' => $model));
            }
        } else
            $this->redirect(array('site/index'));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Utilisateur('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Utilisateur']))
            $model->attributes = $_GET['Utilisateur'];

        $this->renderPartial('admin', array(
            'model' => $model,
        ),false,true);
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Utilisateur the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Utilisateur::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Utilisateur $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'utilisateur-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public static function normalizeChars($s) {
        $replace = array(
            'ъ' => '-', 'Ь' => '-', 'Ъ' => '-', 'ь' => '-',
            'Ă' => 'A', 'Ą' => 'A', 'À' => 'A', 'Ã' => 'A', 'Á' => 'A', 'Æ' => 'A', 'Â' => 'A', 'Å' => 'A', 'Ä' => 'Ae',
            'Þ' => 'B',
            'Ć' => 'C', 'ץ' => 'C', 'Ç' => 'C',
            'È' => 'E', 'Ę' => 'E', 'É' => 'E', 'Ë' => 'E', 'Ê' => 'E',
            'Ğ' => 'G',
            'İ' => 'I', 'Ï' => 'I', 'Î' => 'I', 'Í' => 'I', 'Ì' => 'I',
            'Ł' => 'L',
            'Ñ' => 'N', 'Ń' => 'N',
            'Ø' => 'O', 'Ó' => 'O', 'Ò' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'Oe',
            'Ş' => 'S', 'Ś' => 'S', 'Ș' => 'S', 'Š' => 'S',
            'Ț' => 'T',
            'Ù' => 'U', 'Û' => 'U', 'Ú' => 'U', 'Ü' => 'Ue',
            'Ý' => 'Y',
            'Ź' => 'Z', 'Ž' => 'Z', 'Ż' => 'Z',
            'â' => 'a', 'ǎ' => 'a', 'ą' => 'a', 'á' => 'a', 'ă' => 'a', 'ã' => 'a', 'Ǎ' => 'a', 'а' => 'a', 'А' => 'a', 'å' => 'a', 'à' => 'a', 'א' => 'a', 'Ǻ' => 'a', 'Ā' => 'a', 'ǻ' => 'a', 'ā' => 'a', 'ä' => 'ae', 'æ' => 'ae', 'Ǽ' => 'ae', 'ǽ' => 'ae',
            'б' => 'b', 'ב' => 'b', 'Б' => 'b', 'þ' => 'b',
            'ĉ' => 'c', 'Ĉ' => 'c', 'Ċ' => 'c', 'ć' => 'c', 'ç' => 'c', 'ц' => 'c', 'צ' => 'c', 'ċ' => 'c', 'Ц' => 'c', 'Č' => 'c', 'č' => 'c', 'Ч' => 'ch', 'ч' => 'ch',
            'ד' => 'd', 'ď' => 'd', 'Đ' => 'd', 'Ď' => 'd', 'đ' => 'd', 'д' => 'd', 'Д' => 'D', 'ð' => 'd',
            'є' => 'e', 'ע' => 'e', 'е' => 'e', 'Е' => 'e', 'Ə' => 'e', 'ę' => 'e', 'ĕ' => 'e', 'ē' => 'e', 'Ē' => 'e', 'Ė' => 'e', 'ė' => 'e', 'ě' => 'e', 'Ě' => 'e', 'Є' => 'e', 'Ĕ' => 'e', 'ê' => 'e', 'ə' => 'e', 'è' => 'e', 'ë' => 'e', 'é' => 'e',
            'ф' => 'f', 'ƒ' => 'f', 'Ф' => 'f',
            'ġ' => 'g', 'Ģ' => 'g', 'Ġ' => 'g', 'Ĝ' => 'g', 'Г' => 'g', 'г' => 'g', 'ĝ' => 'g', 'ğ' => 'g', 'ג' => 'g', 'Ґ' => 'g', 'ґ' => 'g', 'ģ' => 'g',
            'ח' => 'h', 'ħ' => 'h', 'Х' => 'h', 'Ħ' => 'h', 'Ĥ' => 'h', 'ĥ' => 'h', 'х' => 'h', 'ה' => 'h',
            'î' => 'i', 'ï' => 'i', 'í' => 'i', 'ì' => 'i', 'į' => 'i', 'ĭ' => 'i', 'ı' => 'i', 'Ĭ' => 'i', 'И' => 'i', 'ĩ' => 'i', 'ǐ' => 'i', 'Ĩ' => 'i', 'Ǐ' => 'i', 'и' => 'i', 'Į' => 'i', 'י' => 'i', 'Ї' => 'i', 'Ī' => 'i', 'І' => 'i', 'ї' => 'i', 'і' => 'i', 'ī' => 'i', 'ĳ' => 'ij', 'Ĳ' => 'ij',
            'й' => 'j', 'Й' => 'j', 'Ĵ' => 'j', 'ĵ' => 'j', 'я' => 'ja', 'Я' => 'ja', 'Э' => 'je', 'э' => 'je', 'ё' => 'jo', 'Ё' => 'jo', 'ю' => 'ju', 'Ю' => 'ju',
            'ĸ' => 'k', 'כ' => 'k', 'Ķ' => 'k', 'К' => 'k', 'к' => 'k', 'ķ' => 'k', 'ך' => 'k',
            'Ŀ' => 'l', 'ŀ' => 'l', 'Л' => 'l', 'ł' => 'l', 'ļ' => 'l', 'ĺ' => 'l', 'Ĺ' => 'l', 'Ļ' => 'l', 'л' => 'l', 'Ľ' => 'l', 'ľ' => 'l', 'ל' => 'l',
            'מ' => 'm', 'М' => 'm', 'ם' => 'm', 'м' => 'm',
            'ñ' => 'n', 'н' => 'n', 'Ņ' => 'n', 'ן' => 'n', 'ŋ' => 'n', 'נ' => 'n', 'Н' => 'n', 'ń' => 'n', 'Ŋ' => 'n', 'ņ' => 'n', 'ŉ' => 'n', 'Ň' => 'n', 'ň' => 'n',
            'о' => 'o', 'О' => 'o', 'ő' => 'o', 'õ' => 'o', 'ô' => 'o', 'Ő' => 'o', 'ŏ' => 'o', 'Ŏ' => 'o', 'Ō' => 'o', 'ō' => 'o', 'ø' => 'o', 'ǿ' => 'o', 'ǒ' => 'o', 'ò' => 'o', 'Ǿ' => 'o', 'Ǒ' => 'o', 'ơ' => 'o', 'ó' => 'o', 'Ơ' => 'o', 'œ' => 'oe', 'Œ' => 'oe', 'ö' => 'oe',
            'פ' => 'p', 'ף' => 'p', 'п' => 'p', 'П' => 'p',
            'ק' => 'q',
            'ŕ' => 'r', 'ř' => 'r', 'Ř' => 'r', 'ŗ' => 'r', 'Ŗ' => 'r', 'ר' => 'r', 'Ŕ' => 'r', 'Р' => 'r', 'р' => 'r',
            'ș' => 's', 'с' => 's', 'Ŝ' => 's', 'š' => 's', 'ś' => 's', 'ס' => 's', 'ş' => 's', 'С' => 's', 'ŝ' => 's', 'Щ' => 'sch', 'щ' => 'sch', 'ш' => 'sh', 'Ш' => 'sh', 'ß' => 'ss',
            'т' => 't', 'ט' => 't', 'ŧ' => 't', 'ת' => 't', 'ť' => 't', 'ţ' => 't', 'Ţ' => 't', 'Т' => 't', 'ț' => 't', 'Ŧ' => 't', 'Ť' => 't', '™' => 'tm',
            'ū' => 'u', 'у' => 'u', 'Ũ' => 'u', 'ũ' => 'u', 'Ư' => 'u', 'ư' => 'u', 'Ū' => 'u', 'Ǔ' => 'u', 'ų' => 'u', 'Ų' => 'u', 'ŭ' => 'u', 'Ŭ' => 'u', 'Ů' => 'u', 'ů' => 'u', 'ű' => 'u', 'Ű' => 'u', 'Ǖ' => 'u', 'ǔ' => 'u', 'Ǜ' => 'u', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'У' => 'u', 'ǚ' => 'u', 'ǜ' => 'u', 'Ǚ' => 'u', 'Ǘ' => 'u', 'ǖ' => 'u', 'ǘ' => 'u', 'ü' => 'ue',
            'в' => 'v', 'ו' => 'v', 'В' => 'v',
            'ש' => 'w', 'ŵ' => 'w', 'Ŵ' => 'w',
            'ы' => 'y', 'ŷ' => 'y', 'ý' => 'y', 'ÿ' => 'y', 'Ÿ' => 'y', 'Ŷ' => 'y',
            'Ы' => 'y', 'ž' => 'z', 'З' => 'z', 'з' => 'z', 'ź' => 'z', 'ז' => 'z', 'ż' => 'z', 'ſ' => 'z', 'Ж' => 'zh', 'ж' => 'zh'
        );
        return strtr($s, $replace);
    }
    
    
    


}
