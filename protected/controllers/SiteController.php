<?php

class SiteController extends Controller {

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        $musiquesrecentes = Musique::model()->with('artiste')->findAll(array('order' => 'dateajout DESC'));
        $recommandations = Musique::model()->with('artiste')->findAll(array('order' => 'note DESC, dateajout DESC'));

        if (!Yii::app()->user->isGuest) {

            $idutil = Yii::app()->user->idutilisateur;

            $criteria1 = new CDbCriteria();
            $criteria1->alias = 'm';
            $criteria1->condition = 'm.idutilisateur=' . $idutil;
            $criteria1->with = array('genre', 'auteur');
            $criteria1->order = 'titre ASC';

            //Récupération les musiques de l'artiste
            $messons = Musique::model()->findAll($criteria1);
            $musiquesfavoris = Musique::getMusiquesFavoris($idutil);

            //On récupère les post des amis mais aussi ses posts à lui
            
            $posts = Yii::app()->user->getPosts(0);
            
            if (Yii::app()->request->isAjaxRequest)
                $this->renderPartial('index', array('messons' => $messons, 'musiquesrecentes' => $musiquesrecentes, 'musiquesfavoris' => $musiquesfavoris, 'posts'=>$posts, 'recommandations'=>$recommandations));
            else
                $this->render('index', array('messons' => $messons, 'musiquesrecentes' => $musiquesrecentes, 'musiquesfavoris' => $musiquesfavoris, 'posts'=>$posts, 'recommandations'=>$recommandations));
        } else {
            if (Yii::app()->request->isAjaxRequest)
                $this->renderPartial('index', array('musiquesrecentes' => $musiquesrecentes, 'recommandations'=>$recommandations));
            else
                $this->render('index', array('musiquesrecentes' => $musiquesrecentes, 'recommandations'=>$recommandations));
        }
    }

    public function actionCollaboration() {
        if (Yii::app()->request->isAjaxRequest)
            $this->renderPartial('pages/collaboration');
        else
            $this->render('pages/collaboration');
    }

    public function actionAbout() {
        if (Yii::app()->request->isAjaxRequest)
            $this->renderPartial('pages/about');
        else
            $this->render('pages/about');
    }

    public function actionUpload() {
        if (Yii::app()->request->isAjaxRequest)
            echo $this->renderPartial('upload', array(), true, true);
        else
            $this->render('upload');
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact() {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                        "Reply-To: {$model->email}\r\n" .
                        "MIME-Version: 1.0\r\n" .
                        "Content-Type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {
        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            $model->username = $_POST['username'];
            $model->password = md5($_POST['password']);

            if ($model->validate()) {
                $model->login();
                $this->redirect(Yii::app()->user->returnUrl);
            } else {
                if ($_POST['username'] == '')
                    $_POST['username'] = 'vide';
                else if ($model->hasErrors('username'))
                    $_POST['username'] = 'incorrect';

                if ($_POST['password'] == '')
                    $_POST['password'] = 'vide';
                else if ($model->hasErrors('password'))
                    $_POST['password'] = 'incorrect';
                $this->redirect('index', array('model' => $model));
            }
        }else {
            $this->render('index');
        }
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

}
